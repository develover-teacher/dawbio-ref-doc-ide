package personas;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/** 
 * ExPractica3. Paraules ofensives. 
 * Crea una llista de paraules ofensives, i analitza si un text separat
 * per paraules.
 */
public class EjListaPalabrasOfensivas {
    
    private static void mostrarLista(List<String> nombresPersonas) {
        for (String nombrePersona : nombresPersonas) {
            System.out.println(nombrePersona);
        }
    }
    
    static List<String> palabrasOfensivas = 
            new ArrayList<String>();
    
    public static void main(String[] args) {
        
        String[] paraules = {"ximple","imbecil","babau","inutil","burro","loser","noob","capsigrany","torrecollons","fatxa","nazi","supremacista"};
        Collections.addAll(palabrasOfensivas, paraules);

//        palabrasOfensivas.add("ximple");
//        palabrasOfensivas.add("imbècil");
//        palabrasOfensivas.add("babau");
//        palabrasOfensivas.add("burro");
        
        System.out.println("Demo EjListaPalabrasOfensivas");
        mostrarLista(palabrasOfensivas);
        
        // Exemples de comentaris a provar.
        String comentarioOK = "Hola que tal va todo?";
        String comentarioImbecil = "Hola que tal va, imbecil?";
        
        validaComentari2(comentarioOK);
        validaComentari2(comentarioImbecil);
    }

    private static void validaComentari(String comentario) {
        // 1. Separem el text per paraules.
        String[] palabrasTexto = comentario.split("\\W+");
        boolean comValido = true;
        String palabraComentario;
        // Recorrem totes les paraules del text
        for (int i = 0;  i < palabrasTexto.length && comValido; i++) {
            palabraComentario = palabrasTexto[i];
            System.out.println("palabraComentario="+palabraComentario);
            // La paraula del comentari es una paraulota ??
            // Si ho és, el comentari ja no serà vàlid.
            comValido = !palabrasOfensivas.contains(palabraComentario);
            System.out.println("comValido="+comValido);
        }
        if(!comValido)
            System.out.println("EL COMENTARIO HA SIDO CENSURADO.");
        else
            System.out.println("EL COMENTARIO ES VÁLIDO.");  
    }
    
    private static void validaComentari2(String comentario) {
        // 1. Separem el text per paraules.
        String[] palabrasTexto = comentario.split("\\W+");
        boolean comValido = true;
        // Recorrem totes les paraules del text
        for (String palabraComentario : palabrasTexto) {
            if(palabrasOfensivas.contains(palabraComentario)) {
                comValido=false;
                break;
            }
        }
        if(!comValido)
            System.out.println("EL COMENTARIO HA SIDO CENSURADO.");
        else
            System.out.println("EL COMENTARIO ES VÁLIDO.");  
    }
}
