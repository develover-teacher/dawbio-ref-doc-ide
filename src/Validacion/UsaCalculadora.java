package Validacion;

import java.util.Arrays;

/**
 * 21. Aplicació de calculadora que mostri un menú amb les opcions sumar, restar, 
 * multiplicar, dividir i sortir.  
 * A continuació demani dos números, i per últim els operi.
 * Si el divisor es 0 ha de mostrar un error per pantalla.
 * @author mique
 */
public class UsaCalculadora {
    
    public static void main(String[] args) {
        
        // sasdasoñdsdasdsda
        String salutacio = "Prova POO Calculadora.";
        System.out.println(salutacio);
        
        Calculadora calc = new Calculadora();
        System.out.println("5+4="+calc.suma(5.0, 4.0));
        
        double result = calc.suma(3,calc.suma(5.0, 4.0));
        System.out.println("5+4+3="+result);
        
        result = calc.div(20.0, 5.0);
        System.out.println("20/5="+result);
    }
}
